#include <QtGui>

 #include "mainwindow.h"

 mainwindow::mainwindow()
 {
     QWidget *widget = new QWidget;
     setCentralWidget(widget);

     P = new Panel(this);

     QVBoxLayout *layout = new QVBoxLayout;
     layout->setMargin(5);
     layout->addWidget(P);
     P->show();
     widget->setLayout(layout);

     createActions();
     createMenus();

     setWindowTitle(tr("Asalto a Sebastopol"));
     setMinimumSize(160, 160);
     resize(1000, 700);
 }

 void mainwindow::nuevo_juego()
 {
	P->Inicializar();
 }

 void mainwindow::cargar()
 {
    P->Cargar_Partida() ;
 }

 void mainwindow::guardar()
 {
     P->Guardar_Partida() ;
 }

 void mainwindow::createActions()
 {
     newAct = new QAction(tr("&Nuevo"), this);
     newAct->setShortcut(tr("Ctrl+N"));
     newAct->setStatusTip(tr("Comienza un nuevo juego"));
     connect(newAct, SIGNAL(triggered()), this, SLOT(nuevo_juego()));

     openAct = new QAction(tr("&Cargar..."), this);
     openAct->setShortcut(tr("Ctrl+C"));
     openAct->setStatusTip(tr("Abre una partida existente"));
     connect(openAct, SIGNAL(triggered()), this, SLOT(cargar()));

     saveAct = new QAction(tr("&Guardar"), this);
     saveAct->setShortcut(tr("Ctrl+G"));
     saveAct->setStatusTip(tr("Guarda la partida en disco"));
     connect(saveAct, SIGNAL(triggered()), this, SLOT(guardar()));

     exitAct = new QAction(tr("&Salir"), this);
     exitAct->setShortcut(tr("Ctrl+S"));
     exitAct->setStatusTip(tr("Terminar el juego"));
     connect(exitAct, SIGNAL(triggered()), this, SLOT(close()));


 }

 void mainwindow::createMenus()
 {
     fileMenu = menuBar()->addMenu(tr("Juego"));
     fileMenu->addAction(newAct);
     fileMenu->addAction(openAct);
     fileMenu->addAction(saveAct);
     fileMenu->addSeparator();
     fileMenu->addAction(exitAct);
 }
