#ifndef mainwindow_H
#define mainwindow_H

#include <QMainWindow>
#include "Panel.h"

 class QAction;
 class QActionGroup;
 class QLabel;
 class QMenu;

 class mainwindow : public QMainWindow
 {
     Q_OBJECT

 public:
     mainwindow();

 private slots:
     void nuevo_juego();
     void cargar();
     void guardar();

 private:
     void createActions();
     void createMenus();

     QMenu *fileMenu;
     QMenu *editMenu;
     QMenu *formatMenu;
     QMenu *helpMenu;
     QActionGroup *alignmentGroup;
     QAction *newAct;
     QAction *openAct;
     QAction *saveAct;
     QAction *exitAct;
     Panel *P;

 };

 #endif

