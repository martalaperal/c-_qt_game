
#include <QtGui>
#include "Panel.h"

void Panel::Inicializar()
{

	//Ponemos el título
	setWindowTitle(tr("Asalto a Sebastopol"));

	// Inicializamos el número de fichas blancas a 20, se irá decrementando según se vayan "comiendo"

	nblancas = 20;

	//Inicializamos las casillas prohibidas a -1

	M[0][0]=-1;
	M[0][1]=-1;
	M[1][0]=-1;
	M[1][1]=-1;

	M[5][0]=-1;
	M[5][1]=-1;
	M[6][0]=-1;
	M[6][1]=-1;

	M[0][5]=-1;
	M[0][6]=-1;
	M[1][5]=-1;
	M[1][6]=-1;

	M[5][5]=-1;
	M[5][6]=-1;
	M[6][5]=-1;
	M[6][6]=-1;

	//Inicializamos las casillas vacías a 0

	M[2][0]=0;
	M[3][0]=0;
	M[4][0]=0;

	M[2][1]=0;
	M[3][1]=0;
	M[4][1]=0;

	M[0][2]=0;
	M[1][2]=0;

	M[3][2]=0;

	M[5][2]=0;
	M[6][2]=0;

	//Inicializamos las casillas con fichas blancas a 1

	M[0][3]=1;
	M[1][3]=1;
	M[2][3]=1;
	M[3][3]=1;
	M[4][3]=1;
	M[5][3]=1;
	M[6][3]=1;

	M[0][4]=1;
	M[1][4]=1;
	M[2][4]=1;
	M[3][4]=1;
	M[4][4]=1;
	M[5][4]=1;
	M[6][4]=1;
	
	M[2][5]=1;
	M[3][5]=1;
	M[4][5]=1;

	M[2][6]=1;
	M[3][6]=1;
	M[4][6]=1;

	//Inicializamos las casillas con fichas negras a 2

	M[2][2]=2;
	M[4][2]=2;

	//Inicializamos las variables coordenadas
	coord_x=coord_y=-1;

	//Inicializamos las variables que marcarán la casilla activa
	C_origen=F_origen=C_destino=F_destino=-1;

	//El turno será para las blancas en primer lugar
	turno=1;

	//Al principio no hay ganador
	ganador=0;

	//Inicializaremos el mensaje de eventos como vacío
	mensaje="Selecciona una ficha de tu color";

	//El primer mensaje que msotraremos será:
	estado="Mueven las blancas";

	//Cargamos las imágenes
	FB.load("Ficha_Blanca.png");
	FN.load("Ficha_Negra.png");
	AB.load("Alternativa_Blanca.png");
	AN.load("Alternativa_Negra.png");
	TAB.load("Tablero.png");

	repaint();
}

Panel::Panel(QWidget *parent):QWidget(parent) 
{
	Inicializar();
}

bool Panel :: Movimiento_Posible (const int &fil_actual, const int &col_actual, const int &fil_destino, const int &col_destino){

	bool permitido = false ;

	if ( fil_destino >= 0 && fil_destino < 8 && col_destino >= 0 && col_destino < 8 )
	{

		// Si la casilla destino es una casilla válida, es decir :
		// - No es una casilla inválida (-1)
		// - No hay una ficha blanca ya en ella (1)
		// - No hay una ficha negra ya en ella (2)
		// Que se puede resumir en que la casilla destino tenga el valor 0, es decir, que sea una casilla vacía
		if ( M[fil_destino][col_destino] == 0)
		{

			//Si el turno es de las blancas
			if(M[fil_actual][col_actual]==1)
			{
				// Comprobamos entonces que sean adyacentes las filas

				//Si se mueve en la misma columna
				if(col_actual == col_destino)
				{
					//Comprobamos que las filas son adyacentes
					if(fil_destino == fil_actual+1 || fil_destino == fil_actual-1)
					{
						permitido=true ; // Declaramos entonces el movimiento como válido
					}
					else
						mensaje="Las casillas deben ser adyacentes";
				}
				//Si se mueve hacia arriba
				else if (col_destino == col_actual-1)
				{
					


					if(fil_destino == fil_actual)
						permitido=true;
					else if(fil_destino == fil_actual+1 || fil_destino == fil_actual-1)
					{
						//Comprobamos que el movimiento está permitido:
						//Si la columna de origen es par no se puede mover de fila impar a par, por lo que la columna de origen y la fila de origen no pueden par a impar
						if (col_actual%2!=fil_actual%2)
						{
							mensaje="Movimiento no permitido. Siga las lineas";
						}
						else
						{
							permitido=true ; // Declaramos entonces el movimiento como válido
						}
					}
					else
						mensaje="Las casillas deben ser adyacentes";
				}
				//Si se quiere retroceder
				else if (col_destino == col_actual+1)
				{
					mensaje="Las blancas no pueden retroceder";
				}
				else
					mensaje="Las casillas deben ser adyacentes";
			}
			//Si el turno es de las negras
			else
			{
				// Comprobamos entonces si son adyacentes las casillas

				//Si se mueve en la misma columna
				if(col_actual == col_destino)
				{
					//Comprobamos que las filas son adyacentes
					if(fil_destino == fil_actual+1 || fil_destino == fil_actual-1)
						permitido = true ; // Declaramos entonces el movimiento como válido
					//Comprobamos si se está intentando comer en la misma columna
					if((fil_destino == fil_actual+2 && M[fil_actual+1][col_actual]==1) || (fil_destino == fil_actual-2 && M[fil_actual-1][col_actual]==1))
						permitido = true ; // Declaramos entonces el movimiento como válido
					else
						mensaje="Las casillas vacias deben ser adyacentes";
				}
				//Si se mueve hacia arriba o hacia abajo
				else if (col_destino == col_actual-1 || col_destino == col_actual+1)
				{
					//Comprobamos que las filas son adyacentes
					if(fil_destino == fil_actual)
						permitido=true;
					else if(fil_destino == fil_actual+1 || fil_destino == fil_actual-1)
					{
						//Comprobamos que el movimiento está permitido:
						//Si la columna de origen es par no se puede mover de fila impar a par, por lo que la columna de origen y la fila de origen no pueden ser par e impar
						if (col_actual%2!=fil_actual%2)
						{
							mensaje="Movimiento no permitido. Siga las lineas";
						}
						else
						{
							permitido=true ; // Declaramos entonces el movimiento como válido
						}
					}
					else
						mensaje="Las casillas vacias deben ser adyacentes";
				}
				//Si se está intentando comer hacia arriba
				else if (col_destino == col_actual-2)
				{
					//Si están en la misma fila no hace falta comprobar si el movimiento está permitido
					//Se comprueba si hay ficha blanca en medio
					if(fil_destino == fil_actual && M[fil_actual][col_actual-1]==1)
						permitido = true;
					//Si sigue las lineas
					if(col_actual%2==fil_actual%2)
					{
						//Comprobaremos si existe una ficha blanca en medio
						if(
						/*diagonal izquierda*/((fil_destino == fil_actual-2 && M[fil_actual-1][col_actual-1]==1) ||
						/*diagonal derecha*/(fil_destino == fil_actual+2 && M[fil_actual+1][col_actual-1]==1)))
							permitido = true ; // Declaramos entonces el movimiento como válido
						else
							mensaje="Las casillas vacias deben ser adyacentes";
					}
					else
						mensaje="Movimiento no permitido. Siga las lineas";
				}
				//Si se está intentando comer hacia abajo
				else if (col_destino == col_actual+2)
				{
					//Si están en la misma fila no hace falta comprobar si el movimiento está permitido
					//Se comprueba si hay ficha blanca en medio
					if(fil_destino == fil_actual && M[fil_actual][col_actual+1]==1)
						permitido = true;
					//Si sigue las lineas
					else if(col_actual%2==fil_actual%2)
					{
						//Comprobaremos si existe una ficha blanca en medio
						if(
						/*diagonal izquierda*/((fil_destino == fil_actual-2 && M[fil_actual-1][col_actual+1]==1) ||
						/*diagonal derecha*/(fil_destino == fil_actual+2 && M[fil_actual+1][col_actual+1]==1)))
							permitido = true ; // Declaramos entonces el movimiento como válido
						else
							mensaje="Las casillas vacias deben ser adyacentes";
					}
					else
						mensaje="Movimiento no permitido. Siga las lineas";
				}
				else
					mensaje="Las casillas deben ser adyacentes";
			}
		}
		else
		{
			mensaje="Debe mover a una casilla vacia";
		}
	}
	else
	{
		mensaje="Selecciona una casilla vacia para mover la ficha";
	}

	return permitido ;
}

void Panel :: Turno_Jugador ()
{

	if (Movimiento_Posible (F_origen, C_origen, F_destino, C_destino))
	{

		M[F_destino][C_destino] = turno ; // La casilla anteriormente vacía queda ocupada por nuestra ficha
		M[F_origen][C_origen] = 0 ; // La casilla de origen la declaramos como vacía

		if ( turno == 2 ) // Si la ficha es negra, comprobamos si se ha comido alguna blanca
		{

			if ( F_origen == F_destino-2 ) // Por debajo de la casilla actual
			{

				if ( C_origen == C_destino-2 ){ // Diagonal derecha
					M[F_origen+1][C_origen+1] = 0 ; // Establecemos que la casilla ahora esta vacia por la comida
					nblancas-- ; // Decrementamos en uno el numero de fichas blancas, por la comida
				}
				else if ( C_origen == C_destino+2 ){ // Diagonal izquierda
					M[F_origen+1][C_origen-1] = 0 ;
					nblancas-- ; // Decrementamos en uno el numero de fichas blancas, por la comida
				}
				else if(C_origen == C_destino){
					M[F_origen+1][C_origen] = 0 ;
					nblancas-- ; // Decrementamos en uno el numero de fichas blancas, por la comida
				}
				else
				{
					turno=1;
					estado="Mueven las blancas";
				}

			}
			else if ( F_origen == F_destino+2 )// Por arriba de la casilla actual
			{
				if ( C_origen == C_destino-2 ){ // Diagonal derecha
					M[F_origen-1][C_origen+1] = 0 ;
					nblancas-- ; // Decrementamos en uno el numero de fichas blancas, por la comida
				}
				else if ( C_origen == C_destino+2 ){ // Diagonal izquierda
					M[F_origen-1][C_origen-1] = 0 ;
					nblancas-- ; // Decrementamos en uno el numero de fichas blancas, por la comida
				}
				else if(C_origen == C_destino){
					M[F_origen-1][C_origen] = 0 ;
					nblancas-- ; // Decrementamos en uno el numero de fichas blancas, por la comida
				}
				else
				{
					turno=1;
					estado="Mueven las blancas";
				}
			}
			else if (F_origen == F_destino)// Estan en la misma fila
			{

				if ( C_origen == C_destino+2 )
				{
					M[F_origen][C_origen-1] = 0 ;
					nblancas-- ; // Decrementamos en uno el numero de fichas blancas, por la comida
				}
				else if ( C_origen == C_destino-2 )
				{
					M[F_origen][C_origen+1] = 0 ;
					nblancas-- ; // Decrementamos en uno el numero de fichas blancas, por la comida
				}
				else
				{
					turno=1;
					estado="Mueven las blancas";
				}

				// Como al principio del todo hemos descartado el caso de filas y columnas iguales
				// me ahorro entonces el "else"
			}
			else
			{
				turno=1;
				estado="Mueven las blancas" ;
			}
		}
		else
		{
			turno=2;
			estado="Mueven las negras";
		}

		//Modificamos los parámetros pertinentes
		F_origen=-1;
		C_origen=-1;

		if(Fin_De_Juego())
		{
			repaint();
			MyDialog *fin = new MyDialog(ganador,this);
			bool boton = fin->exec();
			if ( boton == true )
			  Inicializar() ;
			else
			  exit(-1) ;
		}
		else
		{
			mensaje="Selecciona una ficha de tu color";
		}
	}
}

bool Panel :: Fin_De_Juego (){

	// Si han ganado las fichas negras al comerse todas las blancas
	if ( nblancas == 0 )
	{
		ganador = 2 ;
		return true ;
	}

	else
	{

		int f1, c1, f2, c2 ;
		Busca_Posicion_Negras(f1,c1,f2,c2) ;

		//Comprobamos si la primera ficha puede o no mover
		for(int i=f1-2;i<=f1+2;++i)
			for(int j=c1-2;j<=c1+2;++j)
				if(Movimiento_Posible(f1,c1,i,j))
					return false;

		//Comprobamos si la segunda ficha puede o no mover
		for(int i=f2-2;i<=f2+2;++i)
			for(int j=c2-2;j<=c2+2;++j)
				if(Movimiento_Posible(f2,c2,i,j))
					return false;

		ganador = 1 ;
		return true ;
	}
}

void Panel :: Busca_Posicion_Negras ( int & posf1, int & posc1, int & posf2, int & posc2 ){

      int siguiente = 0 ;

      // Cuando encuentre las dos fichas negras ( siguiente == 2 ), entonces dejara de buscar
      for ( int i=0 ; i<8 && siguiente < 2 ; i++ ){
	for ( int j=0 ; j<8 && siguiente < 2 ; j++ ){

	  if ( M[i][j] == 2 ){
	    if ( siguiente == 0 ){ // Hemos encontrado la primera ficha negra
	      posf1 = i ;
	      posc1 = j ;
	      siguiente++ ;
	    }
	    else{ // Hemos encontrado la segunda ficha negra
	      posf2 = i ;
	      posc2 = j ;
	      siguiente++ ;
	    }
	  }
	}
      }
}

void Panel::paintEvent(QPaintEvent *event)
{
	QPainter painter(this);
	event = event ; // Para que no salga el mensaje de "event" sin uso

	//Creamos la variables punto que nos dirá donde dibujar la imagen
	QPoint punto(0,0);

	//DIBUJAMOS EL TABLERO:
	painter.drawImage(punto,TAB);

	//Dibujamos la ficha del turno
	if(turno==1)
	{
		punto.setX(879-21);
		punto.setY(107-21);
		painter.drawImage(punto,FB);
	}
	else
	{
		punto.setX(879-30);
		punto.setY(107-30);
		painter.drawImage(punto,FN);
	}

	//DIBUJAMOS las lineas que muestran LOS CAMINOS POSIBLES, para que queden por debajo de las fichas
	
	painter.setRenderHint(QPainter::Antialiasing, true);

	int i,j;
	int coord_aux_x=261;
	int coord_aux_y=87;

	painter.setPen(QPen(Qt::red, 2)); //Escogemos el PEN rojo

	if(F_origen!=-1)
	{
		for(i=0;i<7;++i)
		{
			for(j=0;j<7;++j)
			{
				if(Movimiento_Posible(F_origen,C_origen,i,j))
				{
					painter.drawLine(coord_x,coord_y,coord_aux_x,coord_aux_y);
	
					punto.setX(coord_aux_x-50);
					punto.setY(coord_aux_y-50);
					if(turno==1)
						painter.drawImage(punto,AB);
					else
						painter.drawImage(punto,AN);
				}
	
				coord_aux_y+=80;
			}
	
			coord_aux_x+=80;
			coord_aux_y=87;
		}
	}

	//DIBUJAMOS LAS FICHAS:

	coord_aux_x=261;
	coord_aux_y=87;

	
	painter.setPen(QPen(Qt::black, 3)); //Escogemos el PEN negro

	for(i=0;i<7;++i)
	{
		for(j=0;j<7;++j)
		{
			if(M[i][j]==1)
			{
				punto.setX(coord_aux_x-21);
				punto.setY(coord_aux_y-22);
				painter.drawImage(punto,FB);
			}
			else if(M[i][j]==2)
			{
				punto.setX(coord_aux_x-30);
				punto.setY(coord_aux_y-30);
				painter.drawImage(punto,FN);
			}

			if(F_origen!=-1 && Movimiento_Posible(F_origen,C_origen,i,j))
			{
				punto.setX(coord_aux_x-50);
				punto.setY(coord_aux_y-50);
				if(turno==1)
					painter.drawImage(punto,AB);
				else
					painter.drawImage(punto,AN);
			}

			coord_aux_y+=80;
		}

		coord_aux_x+=80;
		coord_aux_y=87;
	}

	painter.drawText(50, 50, estado.c_str());
	painter.drawText(50, 70, mensaje.c_str());
}

void Panel::mousePressEvent ( QMouseEvent * e )
{
	//Si no existe ganador habilitamos el click
	  if (e -> button() == Qt::LeftButton)
	  {
		//Almacenamos las coordenadas del click en variables locales
		int pulsado_x=e->x();
		int pulsado_y=e->y();

		//Valores iniciales para recorrer las casillas
		int coord_aux_x=261;
		int coord_aux_y=87;

		int i,j; //Recorrerán la matriz

		int f_aux=-1, c_aux=-1;

		//Recorremos las casillas hasta que encontremos la clickeada
		for(i=0;i<7;++i)
		{
			for(j=0;j<7;++j)
			{
				//Detectamos la zona de la ficha clickeada (le daremos un margen de 20)
				if(coord_aux_x>=pulsado_x-30 && coord_aux_x <=pulsado_x+30
				&&
				coord_aux_y>=pulsado_y-30 && coord_aux_y <=pulsado_y+30)
				{
					f_aux=i; //Asignamos la fila de la casilla
					c_aux=j; //Asignamos la columna de la casilla

					if(F_origen==-1 && ((turno==1 && M[i][j]==1) || (turno==2 && M[i][j]==2)))
					{
						coord_x=coord_aux_x;
						coord_y=coord_aux_y;
					}
				}

				coord_aux_y+=80;
			}

			coord_aux_x+=80;
			coord_aux_y=87;
		}

		//Si la casilla seleccionada es la casilla origen
		if(F_origen==-1 || C_origen==-1)
		{
			if((turno==1 && M[f_aux][c_aux]==1) || (turno==2 && M[f_aux][c_aux]==2))
			{
				F_origen=f_aux;
				C_origen=c_aux;
				mensaje="Selecciona una casilla a la que mover";
			}
			else
			{
				mensaje="Selecciona una ficha de tu color";
			}
		}
		else //Si la casilla es la de destino
		{
			F_destino=f_aux;
			C_destino=c_aux;

			mensaje="";

			//Intentamos mover:
			Turno_Jugador();
		}
	  }

	//Si pulsamos el botón derecho deseleccionaremos la ficha que queremos mover para poder escoger otra o la misma
	else if(e -> button() == Qt::RightButton)
	{
		mensaje="Selecciona una ficha de tu color";
		F_origen=-1;
		C_origen=-1;
	}

	repaint();
}

Panel::MyDialog::MyDialog(int ganador, QWidget *parent) : QDialog(parent)
{
    this->setObjectName(QString::fromUtf8("MyDialog"));
    this->resize(500, 200);
    this->setWindowTitle(QApplication::translate("MyDialog", "¡FIN DEL JUEGO!", 0, QApplication::UnicodeUTF8));

    layout_labels = new QWidget(this);
    layout_labels->setObjectName(QString::fromUtf8("layout_labels"));
    layout_labels->setGeometry(QRect(60, 20, 281, 77));
    caja_labels = new QHBoxLayout(layout_labels);
    caja_labels->setObjectName(QString::fromUtf8("caja_labels"));
    caja_labels->setContentsMargins(0, 0, 0, 0);
    label = new QLabel(layout_labels);
    label->setObjectName(QString::fromUtf8("label"));
    caja_labels->addWidget(label);

    if(ganador==1)
	label->setText(QApplication::translate("MyDialog", "Ganan las blancas\n\n¿Quieres empezar un nuevo juego?", 0, QApplication::UnicodeUTF8));
    else
	label->setText(QApplication::translate("MyDialog", "Ganan las negras\n\n¿Quieres empezar un nuevo juego?", 0, QApplication::UnicodeUTF8));

    layout_botones = new QWidget(this);
    layout_botones->setObjectName(QString::fromUtf8("layout_botones"));
    layout_botones->setGeometry(QRect(40, 130, 291, 29));
    caja_botones = new QHBoxLayout(layout_botones);
    caja_botones->setObjectName(QString::fromUtf8("layout_botones"));
    caja_botones->setContentsMargins(0, 0, 0, 0);
    spacerItem = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

    caja_botones->addItem(spacerItem);

    okButton = new QPushButton(layout_botones);
    okButton->setObjectName(QString::fromUtf8("okButton"));
    okButton->setText(QApplication::translate("MyDialog", "OK", 0, QApplication::UnicodeUTF8));

    caja_botones->addWidget(okButton);

    cancelButton = new QPushButton(layout_botones);
    cancelButton->setObjectName(QString::fromUtf8("cancelButton"));
    cancelButton->setText(QApplication::translate("MyDialog", "Salir", 0, QApplication::UnicodeUTF8));

    caja_botones->addWidget(cancelButton);

    QWidget::setTabOrder(okButton, cancelButton);
    QMetaObject::connectSlotsByName(this);

    connect(okButton, SIGNAL(clicked()), this, SLOT(accept()));
    connect(cancelButton, SIGNAL(clicked()), this, SLOT(close()));
}


void Panel:: Cargar_Partida(){

	int valor ;
	ifstream fichero ;
	int i=0 ;
	int j=0 ;

	fichero.open("partida.txt") ;

	if ( !fichero.good() ){
		cerr << "problema con fichero\n" ;
		exit(1) ;
	}

	while ( !fichero.eof() ){

		fichero >> valor ;

		if ( !fichero.eof() ){

		  M[i][j] = valor ;

		  if ( j==6 ){
		    i++ ;
		    j=0 ;
		  }
		  else
		    j++ ;
		}
	}

	fichero.close() ;
	repaint() ;
}


void Panel:: Guardar_Partida(){

	string cadena ;
	ofstream fichero ("partida.txt") ;

	if ( !fichero.good() ){
		cerr << "problema con fichero\n" ;
		exit(1) ;
	}

	for ( int i=0 ; i<7 ; i++ ){
	  for ( int j=0 ; j<7 ; j++ )
	    fichero << M[i][j] << "\n" ;
	}

	fichero.close() ;
}




