
#ifndef _PANEL_H_
#define _PANEL_H_

#include <QWidget>
#include <QDialog>
#include <QtGui>

#include <iostream>
#include <fstream>
#include <string>

using namespace std;

class Panel : public QWidget
{
	Q_OBJECT

	private:
		int M[7][7]; //Matriz de fichas
		int nblancas;
		int turno ; // Sera 1 para el jugador de fichas blancas, 2 para el de las negras
		int coord_x, coord_y;
		int C_origen, F_origen;
		int C_destino, F_destino;
		int ganador;
		string estado;
		string mensaje;

		QImage TAB;
		QImage FN;
		QImage FB;
		QImage AB;
		QImage AN;
		QImage SN;
		QImage SB;

		//Clase Dialogo
		class MyDialog : public QDialog
		{

			public:
				MyDialog(int ganador, QWidget *parent = 0);

			private:
				QPushButton *okButton;
				QPushButton *cancelButton;
				QHBoxLayout *caja_labels;
				QHBoxLayout *caja_botones;
				QWidget *layout_labels;
				QWidget *layout_botones;
				QSpacerItem *spacerItem;
				QLabel *label;
		};


	public:
		Panel(QWidget *parent=0);
		void Inicializar();
		bool Movimiento_Posible (const int &fil_actual, const int &col_actual, const int &fil_destino, const int &col_destino) ;
		void Turno_Jugador () ;
		bool Fin_De_Juego () ;
		void Busca_Posicion_Negras ( int & posf1, int & posc1, int & posf2, int & posc2 ) ;
		void Activar_Alternativas();
		void Desactivar_Alternativas();
		void Cargar_Partida() ;
		void Guardar_Partida() ;

	protected:
		void mousePressEvent(QMouseEvent *event);
		void paintEvent(QPaintEvent *event);

};

#endif

